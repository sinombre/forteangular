export class ClienteDto{
    constructor(
        public ClienteId: number,
        public NombreCliente: string,  
        public CorreoElectronico: string,              
        public FechaNacimiento: Date,
        public EstatusCliente: string,        
        public LimiteCredito: number,        
    ){
    }
}