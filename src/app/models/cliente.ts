export class Cliente{
    constructor(
        public ClienteId: number,
        public CorreoElectronico: string,
        public Password: string,
        public Domicilio: string,
        public LimiteCredito: number,
        public EstatusClienteId: number,
        public NombreCompleto: string,
        public FechaDeNacimiento: Date,
        public Edad: number,
        public Eliminado: boolean,
        public CreadoPor: string,
        public CreadoEl: Date,
        public ModificadoPor: string,
        public EliminadoPor: string,
        public ModificadoEl?: Date,
        public EliminadoEl?: Date
    ){

    }
}