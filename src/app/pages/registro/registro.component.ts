import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ClientesService } from 'src/app/services/clientes.service';
import { Cliente } from 'src/app/models/cliente';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styles: []
})
export class RegistroComponent implements OnInit {

  public forma: FormGroup;
  
  private enviado: boolean;

  constructor(private clienteService:ClientesService) 
  {
    this.enviado=false;
  }

  ngOnInit() {
    this.forma= new FormGroup({
      nombre: new FormControl(null, Validators.required),
      correo: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      nacimiento: new FormControl(null, Validators.required),
      estatus: new FormControl(0, Validators.required),
      domicilio: new FormControl(null),
      credito: new FormControl(null),
    });
  }

  Registrar(){
    this.enviado=true; 
    if(this.forma.valid && !this.ValidaFecha() && !this.ValidaEstatus())
    {
      let credito=this.forma.value.credito==null?0:this.forma.value.credito;
      let cliente = new Cliente(
        0, 
        this.forma.value.correo,
        this.forma.value.password,
        this.forma.value.domicilio,
        credito,
        this.forma.value.estatus,
        this.forma.value.nombre,
        this.forma.value.nacimiento,
        18,
        false,
        "",
        new Date(),
        "",
        "");      
      this.clienteService.RegistraCliente(cliente)
      .then(data =>
        { 
          if(data.status===200)
          {
            Swal.fire('Correcto!', 'El Cliente ha sido dado de alta', 'success');
            this.forma.reset();
            this.enviado=false;
          }
          else
          {            
            data.json().then(res =>
              {
                Swal.fire('error', res, 'error');                
              })             
          }        
        })
        .catch(reason => Swal.fire('error', 'error inesperado, vuelve a intentarlo', 'error'));
    }
    else
    {
      Swal.fire('Cuidado',
      'existen campos que no han pasado la validacion, revisa la parte de abajo',
      'info');
    }  
  }

  Valida(campo:string):boolean{
    if(this.forma.get(campo).invalid && this.enviado){
      return true;
    }
    else{
      return false;
    }
  }

  ValidaFecha():boolean{                    
    if(this.enviado)
    {
      if(this.forma.value.nacimiento)
      {
        let momento=this.forma.value.nacimiento.split('-');
        let ahora=new Date(momento[0], momento[1]-1, momento[2]);
        return ahora>new Date();
      }
      else
      {        
        return true;
      }      
    }
    return false;   
  }

  ValidaEstatus():boolean
  {
    let estado=parseInt(this.forma.value.estatus, 10);
    if(this.enviado)
    {
      if(estado>0 && estado<4)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    else
    {
      return false;
    }
  }
}
