import { Component, OnInit } from '@angular/core';
import { ClienteDto } from 'src/app/models/clienteDto';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styles: []
})
export class ClienteListComponent implements OnInit {

  public clienteList:ClienteDto[]=[];

  constructor(private clienteService:ClientesService) { }

  ngOnInit() {
    this.GetClientes();
  }

  GetClientes(){       
    this.clienteService.GetClienteList().then((data) => 
      {
        if(data.status===200)
        {
          data.json().then(res => this.clienteList=res);          
        }
        else
        {
          data.json().then(res =>
          Swal.fire('error',
          res.ExceptionMessage,
          'error'));
        }        
      }).catch(reason => 
        {          
          Swal.fire('error', 'No se pudo obtener el listado de clientes', 'error');
        }); 
  }
}
