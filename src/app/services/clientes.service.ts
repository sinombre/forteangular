import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import Swal from 'sweetalert2';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private url:string="http://localhost:56550/api/clientes";
  
  constructor(private http: HttpClient) 
  {     
  }

  RegistraCliente(cliente:Cliente):Promise<Response>
  {    
    return new Promise((resolve, reject) => 
      fetch(this.url,{
        method: 'POST', 
        body: JSON.stringify(cliente),
        headers: {
          'Content-Type': 'application/json'          
        }      
      }).then(resp => resolve(resp))
      .catch(function(error) {
        reject(error);        
      })
      );
  }

  GetClienteList():Promise<Response>
  {
    return new Promise((resolve, reject) =>  
    fetch(this.url)
    .then(resp => resolve(resp))
    .catch(error => reject(error))
    );   
  }  
}
